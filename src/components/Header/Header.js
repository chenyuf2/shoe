import styles from "./Header.module.scss";
import { logo } from "../Logo/Logo";
import { Tween } from "react-gsap";
import { Expo } from "gsap";
const Header = () => {
  return (
    <nav
      className={[
        "nav nav-bar position-absolute header-container d-flex align-items-center justify-content-between container pt-3",
        styles["header-container"],
      ].join(" ")}
    >
      <Tween
        from={{
          delay: 0.1,
          opacity: 0,
          y: 20,
          ease: Expo.easeInOut,
        }}
        stagger={0.25}
      >
        <div>{logo}</div>
        <div>
          <i className="fa fa-bars" style={{ fontSize: "1.5rem" }}></i>
        </div>
      </Tween>
    </nav>
  );
};

export default Header;
