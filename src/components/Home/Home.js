import React, { Suspense, useRef, useState, useEffect } from "react";
import { Canvas, useFrame } from "@react-three/fiber";
import {
  ContactShadows,
  Environment,
  useGLTF,
  OrbitControls,
} from "@react-three/drei";
import { HexColorPicker } from "react-colorful";
import { proxy, useSnapshot } from "valtio";
import shoeDraco from "../../elements/shoe-draco.glb";
import royal from "../../elements/royal_esplanade_1k.hdr";
import styles from "./Home.module.scss";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { Tween, TweenMax } from "react-gsap";
import { Expo, gsap } from "gsap";

const state = proxy({
  current: null,
  items: {
    laces: "#ffffff",
    mesh: "#ffffff",
    caps: "#ffffff",
    inner: "#ffffff",
    sole: "#ffffff",
    stripes: "#ffffff",
    band: "#ffffff",
    patch: "#ffffff",
  },
});

const Shoe = () => {
  const ref = useRef();
  const snap = useSnapshot(state);
  // Drei's useGLTF hook sets up draco automatically, that's how it differs from useLoader(GLTFLoader, url)
  // { nodes, materials } are extras that come from useLoader, these do not exist in threejs/GLTFLoader
  // nodes is a named collection of meshes, materials a named collection of materials
  const { nodes, materials } = useGLTF(shoeDraco);
  console.log(nodes, materials);
  // Animate model
  useFrame((state) => {
    const t = state.clock.getElapsedTime();
    ref.current.rotation.z = -0.2 - (1 + Math.sin(t / 1.5)) / 20;
    ref.current.rotation.x = Math.cos(t / 4) / 8;
    ref.current.rotation.y = Math.sin(t / 4) / 8;
    ref.current.position.y = (1 + Math.sin(t / 1.5)) / 10;
  });

  useEffect(() => {
    gsap.from(document.getElementById("canvas"), {
      opacity: 0,
      y: -580,
      delay: 3,
      ease: Expo.easeInOut,
    });
  }, [ref]);
  // Cursor showing current color
  // const [hovered, set] = useState(null)
  // useEffect(() => {
  //   const cursor = `<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg"><g clip-path="url(#clip0)"><path fill="rgba(255, 255, 255, 0.5)" d="M29.5 54C43.031 54 54 43.031 54 29.5S43.031 5 29.5 5 5 15.969 5 29.5 15.969 54 29.5 54z" stroke="#000"/><g filter="url(#filter0_d)"><path d="M29.5 47C39.165 47 47 39.165 47 29.5S39.165 12 29.5 12 12 19.835 12 29.5 19.835 47 29.5 47z" fill="${snap.items[hovered]}"/></g><path d="M2 2l11 2.947L4.947 13 2 2z" fill="#000"/><text fill="#000" style="white-space:pre" font-family="Inter var, sans-serif" font-size="10" letter-spacing="-.01em"><tspan x="35" y="63">${hovered}</tspan></text></g><defs><clipPath id="clip0"><path fill="#fff" d="M0 0h64v64H0z"/></clipPath><filter id="filter0_d" x="6" y="8" width="47" height="47" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="2"/><feGaussianBlur stdDeviation="3"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/><feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs></svg>`
  //   const auto = `<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill="rgba(255, 255, 255, 0.5)" d="M29.5 54C43.031 54 54 43.031 54 29.5S43.031 5 29.5 5 5 15.969 5 29.5 15.969 54 29.5 54z" stroke="#000"/><path d="M2 2l11 2.947L4.947 13 2 2z" fill="#000"/></svg>`
  //   document.body.style.cursor = `url('data:image/svg+xml;base64,${btoa(hovered ? cursor : auto)}'), auto`
  // }, [hovered])

  // Using the GLTFJSX output here to wire in app-state and hook up events
  return (
    <group
      ref={ref}
      dispose={null}
      // onPointerOver={(e) => (e.stopPropagation(), set(e.object.material.name))}
      // onPointerOut={(e) => e.intersections.length === 0 && set(null)}
      onPointerMissed={() => (state.current = null)}
      onPointerDown={(e) => (
        e.stopPropagation(), (state.current = e.object.material.name)
      )}
    >
      <mesh
        geometry={nodes.shoe.geometry}
        material={materials.laces}
        material-color={snap.items.laces}
      />
      <mesh
        geometry={nodes.shoe_1.geometry}
        material={materials.mesh}
        material-color={snap.items.mesh}
      />
      <mesh
        geometry={nodes.shoe_2.geometry}
        material={materials.caps}
        material-color={snap.items.caps}
      />
      <mesh
        geometry={nodes.shoe_3.geometry}
        material={materials.inner}
        material-color={snap.items.inner}
      />
      <mesh
        geometry={nodes.shoe_4.geometry}
        material={materials.sole}
        material-color={snap.items.sole}
      />
      <mesh
        geometry={nodes.shoe_5.geometry}
        material={materials.stripes}
        material-color={snap.items.stripes}
      />
      <mesh
        geometry={nodes.shoe_6.geometry}
        material={materials.band}
        material-color={snap.items.band}
      />
      <mesh
        geometry={nodes.shoe_7.geometry}
        material={materials.patch}
        material-color={snap.items.patch}
      />
    </group>
  );
};

const Home = () => {
  return (
    <>
      <Header />

      <Canvas
        id="canvas"
        concurrent
        pixelRatio={[1, 2]}
        camera={{ position: [0, 0, 2] }}
        className={styles["shoe-container"]}
      >
        <ambientLight intensity={0.3} />
        <spotLight
          intensity={0.3}
          angle={0.1}
          penumbra={1}
          position={[5, 25, 20]}
        />
        <Suspense fallback={null}>
          <Shoe />
          <Environment files={royal} />
          <ContactShadows
            rotation-x={Math.PI / 2}
            position={[0, -0.8, 0]}
            opacity={0.25}
            width={10}
            height={10}
            blur={2}
            far={1}
          />
        </Suspense>
        {/* <OrbitControls
          minPolarAngle={Math.PI / 2}
          maxPolarAngle={Math.PI / 2}
          enableZoom={false}
          enablePan={false}
        /> */}
      </Canvas>

      {/* <Picker /> */}
      <div
        className={[
          styles["title-container"],
          "position-absolute d-flex flex-column justify-content-center align-items-center",
        ].join(" ")}
      >
        <div>
          <Tween
            from={{
              delay: 0.37,
              opacity: 0,
              y: 20,
              ease: Expo.easeInOut,
            }}
            stagger={0.25}
          >
            <div className="text-right w-100 font-weight-bold h1 m-0 Neue-compressed-font text-uppercase">
              Nike Air_
            </div>
            <div
              className="Neue-compressed-font"
              style={{ color: "#e8e3e3", fontSize: "25vw", lineHeight: "0.8" }}
            >
              VORTEX
            </div>
          </Tween>
          <div className="text-left h5 m-0 Neue-compressed-font mt-3">
            <Tween
              from={{
                delay: 0.8,
                opacity: 0,
                y: 20,
                ease: Expo.easeInOut,
              }}
              stagger={0.25}
            >
              <div>The Nike Vortex get remembered to conquer wet.</div>
              <div>
                A water-repellest upper keeps your feet dry. Reflective propular
                along with an outside
              </div>
              <div>
                designed for wet surfaces. Let you run with confidence despite
                with weather
              </div>
            </Tween>
          </div>
          <div className="mt-2">
            <Tween
              from={{
                delay: 1.4,
                opacity: 0,
                y: 20,
                ease: Expo.easeInOut,
              }}
              stagger={0.15}
            >
              <i className="fa fa-star text-gold "></i>
              <i className="fa fa-star ml-3 text-gold "></i>
              <i className="fa fa-star ml-3 text-gold "></i>
              <i className="fa fa-star ml-3 text-gold "></i>
              <i className="fa fa-star ml-3 text-secondary"></i>
            </Tween>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Home;
