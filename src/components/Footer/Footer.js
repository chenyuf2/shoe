import styles from "./Footer.module.scss";
import { Tween } from "react-gsap";
import { Expo } from "gsap";
const Footer = () => {
  return (
    <div
      className={[
        styles["footer-container"],
        "position-absolute container pb-4 d-flex justify-content-between align-items-center",
      ].join(" ")}
    >
      <Tween
        from={{
          delay: 2,
          opacity: 0,
          y: 20,
          ease: Expo.easeInOut,
        }}
        stagger={0.25}
      >
        <div className="font-weight-bold d-flex align-items-center">
          <i className="fa fa-arrow-left"></i> <div className="ml-3">BACK</div>
        </div>
        <div className="font-weight-bold">THE VORTEX</div>
        <div>
          <i className="fa fa-facebook"></i>
          <i className="fa fa-twitter ml-3"></i>
          <i className="fa fa-instagram ml-3"></i>
        </div>
      </Tween>
    </div>
  );
};

export default Footer;
